<?php
require_once 'htmlfunc.php';
insertHeader('Shout - Megaphone', 'shout')
?>

<div class = "container">
<div id = "response" class = "alert">
</div>
<div class = "panel panel-default panel-form">
<div class = "panel-body">
<h1 class = "centered">Shout!</h1>
<form action="newpost.php" method="post" id="newpost" role = "form">
	<textarea rows = "3" class = "form-control" placeholder="Message" 
			name = "message" id = "message"></textarea>
<div id="mapholder"></div>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBn3wQK3KG2UvDVCMuak9Vqftuy5FmrCHw&sensor=false"></script>
	<input type = "hidden" name = "latitude" id = "latitude">
	<input type = "hidden" name = "longitude" id = "longitude">
	<input type = "hidden" name = "viewRadius" id = "viewRadius" value = "1600">
	<input type = "hidden" name = "authToken" id = "authToken" value = "<?php echo $_COOKIE['megaAuthToken']; ?>">
	<label for  = "time-select" style = "text-align: center">Vanish After</label>
	<select name="expiry" id = "expiry" class="form-control">
	<br>
	<?php
	echo '<option value="'.(1 * 60).'">1 hour</option>';
	for ($i = 2; $i <= 24; $i++)
	{
		echo '<option value="'.($i * 60).'">'.$i.' hours</option>';
	} 
		?>
	</select>
	<input type="submit" value="Submit" class = "btn btn-lg btn-block btn-danger">
</form>
</div>
</div>
</div>	
<?php insertJavascript(); ?>
<script src = "js/newpost.js"></script>
<script src = "js/lib/locationtools.js"></script>
<?php insertEndTags(); ?>