<?php 

require_once 'htmlfunc.php';
insertHeader('Megaphone', 'newuser');
?>


<?php

?>
<div class = "container">
<div id="response" class = "alert"> </div>
<div class = "panel panel-default panel-form">
<div class = "panel-body">
<h1 class = "centered">Sign Up</h1>
<form action="adduser.php" method="post" id="addUserForm" role = "form">
	<input type="text" name="name" id = "name" class = "form-control" placeholder = "First and Last Name">
	<input type="text" name="username" id = "username" class = "form-control" placeholder = "Username">
	<input type="password" name="password" id = "password" class = "form-control" placeholder = "Password">
	<input type="password" name="password2" id = "password2" class = "form-control" placeholder = "Confirm Password">
	<input type="email" name="email" id = "email" class = "form-control" placeholder = "Email">
	<input type="email" name="email2" id = "email2" class = "form-control" placeholder = "Confirm Email">
	<input type="submit" value="Submit" class = "btn btn-lg btn-block btn-danger" value = "Sign Up">
</form>
</div>
</div>
</div>
<?php insertJavascript(); ?>

    <script>
      $(function () {
        $('#addUserForm').on('submit', function (e) {
          $.ajax({
            type: 'post',
            url: 'adduser.php',
            data: $('#addUserForm').serialize(),
            success: function (data) 
            {
				$('#response').html(data);
				if (data.indexOf('Error!') != -1)
				{
					$('#response').addClass('alert-danger');
				}
				else
				{
					$('#response').addClass('alert-success');
					setTimeout(function(){window.location = 'login.php';},3000);
    	        }
    	    }
          });
          e.preventDefault();
        });
      });
    </script>
<?php insertEndTags(); ?>