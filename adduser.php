<?php
	$response = "";
	if(empty($_POST["name"]))
	{
		$response .= "You've left the Name Field Blank.<br />";
	}
	if(!preg_match("/^[A-Za-z0-9-_\",'\s]+$/",$_POST["name"])||!ctype_alnum($_POST["username"])||!ctype_alnum($_POST["password"])||!ctype_alnum($_POST["password"]))
	{
		$response .= "Please enter only alphanumeric characters in the input fields.<br />";
	}
	if($_POST["password"]!=$_POST["password2"])
	{
		$response .= "The passwords you entered don't match.<br />";
	}
	if($_POST["email"]!=$_POST["email2"])
	{
		$response .= "The emails you entered don't match.<br />";
	}

	if($response != "")
	{
		echo "Error! " . $response;
	}
	else
	{
		$name = $_POST["name"];
		$username = $_POST["username"];
		$password = $_POST["password"];
		$email = $_POST["email"];
			
		require(__DIR__.DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR."password.php");
		$password = password_hash($password, PASSWORD_BCRYPT);
		require(__DIR__.DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."config.php");
		
		try
		{
			$db = new PDO("mysql:dbname=$db_database;host=$db_server", $db_username, $db_password);
			$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
			$selectQuery = $db->prepare("SELECT `username`, `email` FROM $db_userTable WHERE username = :username OR email = :email");  
			$data = array( 'username' => $username,  'email' => $email ); 
			$selectQuery->execute($data); 
			
			$selectQuery->setFetchMode(PDO::FETCH_ASSOC);  
			
			while($row = $selectQuery->fetch()) {  
				if($row['username']==$username)
				{
					$response .= 'The Username Entered is already in use.';
				}
				if($row['email']==$email)
				{
					$response .= 'The email is already associated with another account.';
				}
			}
			
			if($response != "")
			{
				echo "Error! " . $response;
			}
			else
			{
				$data = array( 'username' => $username, 'password' => $password, 'email' => $email, 'name' => $name ); 
				$insertQuery = $db->prepare("INSERT INTO $db_userTable ( username, password, email, name ) values ( :username, :password, :email, :name )");  
				if($insertQuery->execute($data) == 1)
				{
					echo "User " . $username . " added successfully.";
				}
			}
			
				
			
			
		}	catch (PDOException $ex) 
		{
		  echo 'MySQL Connection failed: ' . $ex->getMessage();  
		}
	}
?>