-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.5.27 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for mega
CREATE DATABASE IF NOT EXISTS `mega` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mega`;


-- Dumping structure for table mega.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poster` int(11) NOT NULL DEFAULT '0',
  `message` varchar(160) NOT NULL DEFAULT '0',
  `latitude` double NOT NULL DEFAULT '361',
  `longitude` double NOT NULL DEFAULT '361',
  `viewRadius` varchar(50) NOT NULL DEFAULT '0',
  `postTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiryTime` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`),
  KEY `expiryTime` (`expiryTime`,`latitude`,`longitude`,`viewRadius`,`postTime`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mega.posts: ~1 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
REPLACE INTO `posts` (`id`, `poster`, `message`, `latitude`, `longitude`, `viewRadius`, `postTime`, `expiryTime`) VALUES
	(1, 50, 'KILL ALL HUMANS', 40, 40, '100', '2013-10-26 04:12:14', '2013-10-26 06:12:14');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;


-- Dumping structure for table mega.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(65) NOT NULL DEFAULT '0',
  `authToken` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- Dumping data for table mega.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `name`, `email`, `username`, `password`, `authToken`) VALUES
	(50, 'Albert', 'ashaw596@gmail.com', 'ashaw596', '$2y$10$zQQYc9K0e5I2ygIKq.l9ZewBcsc0lKN0EBGxfQmJiXsIiyOsoRQUa', 'ashaw596526b5c9c72c150.67401327'),
	(51, 'asd asdf a3', 'asd@asd.com', 'asd', '$2y$10$Ryhdpsu5vP2lVj5JaX2HBuaRaJ7yp9bgCoCahwHVbFvInqZL.mHhy', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
