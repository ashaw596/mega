<?php 

require_once 'htmlfunc.php';
if ($login != null)
{
	header("Location: nearme.php");
}
insertHeader('Megaphone', 'login');
?>
<div class = "container">
<div id="response" class = "alert"> </div>
<div class = "panel panel-default panel-form">
<div class = "panel-body">
<h1 class = "centered">Login</h1>
<form action="/api/login.php" method="post" id="loginForm" role = "form">
	<input type="text" name="username" id = "username" placeholder = "Username" class = "form-control">
	<input type="password" name="password" id = "password" placeholder = "Password" class = "form-control">
	<input type="submit" value="Login" class = "btn btn-lg btn-block btn-danger">
</form>
</div>
</div>
</div>	
<?php insertJavascript(); ?>

	<script src="./js/lib/cookies.min.js" ></script>
	<script src="./js/login.js" /></script>
<?php insertEndTags(); ?>