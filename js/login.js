 $(function () {
        $('#loginForm').on('submit', function (e) {
          $.ajax({
            type: 'post',
            url: './api/login.php',
            data: $('#loginForm').serialize(),
            success: function (data) {
				if(data.indexOf("Success! ")==0)
				{
					
					Cookies.set('megaAuthToken', data.replace("Success! ","") , { expires: 18000 });
					$('#response').html("Login successful");
					$('#response').addClass("alert-success");
					window.location = "nearme.php"; 

					
				} else if(data.indexOf("Failure! ")==0 )
				{
					$('#response').html(data.replace("Failure! ",""));
					$('#response').addClass("alert-danger");
					
				} else {
				
					$('#response').html(data);
				
				}
				
              //alert('form was submitted');
            }
          });
          e.preventDefault();
        });
      });