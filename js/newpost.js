 $(function () {
        $('#newpost').on('submit', function (e) {
          $.ajax({
            type: 'post',
            url: './api/newpost.php',
            data: $('#newpost').serialize(),
            success: function (data) {
				if(data.indexOf("Success! ")==0)
				{
					$('#response').html("Post Successful!");
					$('#response').addClass("alert-success");
					$('#message').val('');
					window.location = 'nearme.php';

					
				} else if(data.indexOf("Failure! ")==0 )
				{
					$('#response').html(data.replace("Failure! ",""));
					$('#response').addClass("alert-danger");
					
				} else {
				
					$('#response').html(data);
				
				}
				
             // alert('form was submitted');
            }
          });
          e.preventDefault();
        });
      });