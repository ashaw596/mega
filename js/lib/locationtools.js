var x;
$( document ).ready(function()
{ 
x=document.getElementById("demo");
getLocation();
});
function getLocation()
  {
  if (navigator.geolocation)
    {
    navigator.geolocation.getCurrentPosition(showPosition,showError);
    }
  else{x.innerHTML="Geolocation is not supported by this browser.";}
  }
function showPosition(position)
  {
  lat=position.coords.latitude;
  lon=position.coords.longitude;
  latlon=new google.maps.LatLng(lat, lon)
  mapholder=document.getElementById('mapholder')
  mapholder.style.width='100%';
    mapholder.style.height='200px';

  var myOptions={
  center:latlon,zoom:12,
  mapTypeId:google.maps.MapTypeId.ROADMAP,
streetViewControl: false,   
mapTypeControl:false,
  navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
  };
  var map=new google.maps.Map(document.getElementById("mapholder"),myOptions);
  var marker=new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
  document.getElementById('latitude').value = lat;
document.getElementById('longitude').value = lon;
var circle;
circle = new google.maps.Circle({
  map: map,
  strokeWeight: 2,
  radius: 1600,    // 10 miles in metres
  fillColor: '#AA0000',
  editable:true
});
circle.bindTo('center', marker, 'position');
google.maps.event.addListener(circle, 'radius_changed', function () {
    document.getElementById('viewRadius').value = (circle.getRadius());
});
google.maps.event.addListener(circle, 'center_changed', function () {
    document.getElementById('viewRadius').value = (circle.getRadius());
    document.getElementById('latitude').value = circle.getCenter().lat();
	document.getElementById('longitude').value = circle.getCenter().lng();
});
  }
  function showError(error)
  {
  switch(error.code) 
    {
    case error.PERMISSION_DENIED:
      x.innerHTML="User denied the request for Geolocation."
      break;
    case error.POSITION_UNAVAILABLE:
      x.innerHTML="Location information is unavailable."
      break;
    case error.TIMEOUT:
      x.innerHTML="The request to get user location timed out."
      break;
    case error.UNKNOWN_ERROR:
      x.innerHTML="An unknown error occurred."
      break;
    }
  }
