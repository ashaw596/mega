var x;
var nearMe_lat;
var nearMe_lng;
$( document ).ready(function()
{ 
x=document.getElementById("allcards");
getLocation();
});
function getLocation()
  {
  if (navigator.geolocation)
    {
    navigator.geolocation.getCurrentPosition(showPosition,showError);
    }
  else{x.innerHTML="Geolocation is not supported by this browser.";}
  }
  var map;
function showPosition(position)
  {
  lat=position.coords.latitude;
  nearMe_lat = lat;
  lon=position.coords.longitude;
  nearMe_lng = lon;
  latlon=new google.maps.LatLng(lat, lon)
  mapholder=document.getElementById('mapholder')
  mapholder.style.width='100%';
    mapholder.style.height='200px';
      document.getElementById('latitude').value = lat;
document.getElementById('longitude').value = lon;

  var myOptions={
  center:latlon,zoom:12,
  mapTypeId:google.maps.MapTypeId.ROADMAP,
streetViewControl: false, 
  mapTypeControl:false,
  navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
  };
  map =new google.maps.Map(document.getElementById("mapholder"),myOptions);
  var marker=new google.maps.Marker({position:latlon,map:map,title:"You are here!", draggable:true});
/*var circle2;
circle2 = new google.maps.Marker({
  map: map,
  strokeWeight: 2,
  //radius: 1600,    // 10 miles in metres
  //fillColor: '#AA0000',
  draggable:true
});*/

//marker.bindTo('center', marker, 'position');
google.maps.event.addListener(marker, 'dragend', function () {
    //document.getElementById('viewRadius').value = (circle2.getRadius());
        document.getElementById('latitude').value = marker.getPosition().lat();
	document.getElementById('longitude').value = marker.getPosition().lng();

    document.getElementById('allcards').innerHTML = "";
    regenerateListing();
	
	
    
});
/*google.maps.event.addListener(circle2, 'center_changed', function () {
    document.getElementById('viewRadius').value = (circle2.getRadius());
    document.getElementById('latitude').value = circle2.getCenter().lat();
	document.getElementById('longitude').value = circle2.getCenter().lng();
	    document.getElementById('allcards').innerHTML = "";
	    regenerateListing();
});*/
regenerateListing();
  }
  function showError(error)
  {
  switch(error.code) 
    {
    case error.PERMISSION_DENIED:
      x.innerHTML="User denied the request for Geolocation."
      break;
    case error.POSITION_UNAVAILABLE:
      x.innerHTML="Location information is unavailable."
      break;
    case error.TIMEOUT:
      x.innerHTML="The request to get user location timed out."
      break;
    case error.UNKNOWN_ERROR:
      x.innerHTML="An unknown error occurred."
      break;
    }
  }
  var markers = new Array();
  function regenerateListing()
  {
  	$.ajax({
            type: 'post',
            url: './api/getshout.php',
            data: $('#getshout').serialize(),
            success: function (data) {
            var arr = JSON.parse(data);
            if (arr.length == 0)
            	document.getElementById('allcards').innerHTML += "<h1 class = 'centered'>No Messages</h1>";
            else
            {
				
				for (var x = 0; x < arr.length; x++)
				{
					//alert(arr[x].latitude + " " + arr[x].longitude);
					latlon=new google.maps.LatLng(arr[x].latitude, arr[x].longitude);
					//markers[x]=new google.maps.Marker({position:latlon,map:map,title:"pin!"});
					document.getElementById('allcards').innerHTML += "<div class = 'panel panel-default panel-infocard'><p class = 'username'>" + arr[x].name + " says...</p><p class = 'message'>" + arr[x].message + "</p></div>";
				}
            }
				Console.log('form submitted');
            }
          });
  }
  
