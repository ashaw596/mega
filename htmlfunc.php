<?php
// html functions
require_once(__DIR__.DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR."checklogin.php");
$login = checkLogin();
function insertHeader($pageTitle, $pageID)
{
global $login;
echo '<!DOCTYPE html>
<html>
  <head>
    <title>'.$pageTitle.'</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
    <link rel="apple-touch-icon" href="apple-touch-icon-precomposed.png">
    <!-- Bootstrap -->
<link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700,400italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet">
    <link rel = "stylesheet" href = "css/all.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class = "'.$pageID.'">
  <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><i class="fa fa-bullhorn fa-lg"></i> Megaphone</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            '; if ($login == null) { echo '<li id = "index"><a href="index.php"><i class="fa fa-home"></i> Home</a></li>';
            }
            echo '
            <li id = "near-me"><a href="nearme.php"><i class="fa fa-location-arrow"></i> Near Me</a></li>
            <!--<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <<ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>-->
          </ul>
          <ul class="nav navbar-nav navbar-right">
          ';
    		if ($login) {
    		echo '
          <li id = "shout"><a href="shout.php"><i class="fa fa-comment-o"></i> Shout</a></li>
          <li id = "logout"><a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>';
          }
          else
          {
          	echo '
          	<li id = "newuser"><a href="newuser.php"><i class="fa fa-pencil-square-o"></i> Sign Up</a></li>
          	<li id = "login"><a href="login.php"><i class="fa fa-sign-in"></i> Login</a></li>';
          }
          echo '</ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>';
}
function insertJavascript()
{
	echo '    <!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="js/lib/fastclick.js"></script>
	<script src="js/navactive.js"></script>
    ';
}
function insertEndTags()
{
echo '
<div class = "footer"><span class = "footer">&#169; 2013 Jonathan Jemson and Albert Shaw.  Developed at the GIT MAD/GT WebDev App-a-Thon</span></div>
</body>
</html>';

}
function insertFooter()
{
	insertJavascript();
	insertEndTags();
}
?>