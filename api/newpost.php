<?php
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR."checklogin.php");
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."api".DIRECTORY_SEPARATOR."authenticate.php");
	$key=@$_POST["key"];
	if(checkIdentity($key)<1)
	{
		echo "Failure! You don't permssion to create posts";
		
	} else
	{
		try 
		{
			if(empty($_POST["authToken"]))
			{
				throw new InvalidArgumentException('The authentication given is empty');
			}
			
			$authToken = $_POST["authToken"];
			
			$login = checklogin($authToken);
			
			if(!($login))
			{
				echo "Failure! Authentication failed.";
			}
			else
			{
				if(empty($_POST["message"]))
				{
					throw new InvalidArgumentException('The message given is empty');
				}
				else
				{
					$message = $_POST["message"];
				}
				
				if(empty($_POST["viewRadius"]))
				{
					$viewRadius = 0;
				}
				else
				{
					$viewRadius = (int) $_POST["viewRadius"];
				}
				
				if(empty($_POST["latitude"]))
				{
					throw new InvalidArgumentException('The latitude given is invalid');
				}
				else
				{
					$latitude = (double) $_POST["latitude"];
				}
				
				if(empty($_POST["longitude"]))
				{
					throw new InvalidArgumentException('The longitude given is invalid');
				}
				else
				{
					$longitude = (double) $_POST["longitude"];
				}
				
				if(empty($_POST["expiry"]))
				{
					$expiry = 60;
				}
				else
				{
					$expiry = (int) $_POST["expiry"];
				}
				
				$latitude = deg2rad($latitude);
				$longitude = deg2rad($longitude);
				
				$expiry = $expiry * 60;
				
				$time = time();
				
				$expiry += $time; 
				
				$userid = $login["id"];
				
				$db = new PDO("mysql:dbname=$db_database;host=$db_server", $db_username, $db_password);
				$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
				$data = array( 'poster' => $userid, 'message' => $message, 'latitude' => $latitude, 
								'longitude' => $longitude, 'viewRadius' => $viewRadius, 'postTime' => $time, 
								'expiryTime' =>  $expiry ); 
				$insertQuery = $db->prepare("INSERT INTO $db_postTable ( poster, message, latitude, longitude, viewRadius, postTime, expiryTime ) 
											values ( :poster, :message, :latitude, :longitude, :viewRadius, FROM_UNIXTIME(:postTime), FROM_UNIXTIME(:expiryTime) )");  
				
				
				if($insertQuery->execute($data) == 1)
				{	
					$id = $db -> lastInsertId();
					echo "Success! Post id:$id successfully posted.";
				}
				
				
			}
		} 
		catch (InvalidArgumentException $ex)
		{
			echo 'Invalid Input value: ' . $ex->getMessage();  
		} 
		catch (PDOException $ex) 
		{
		  echo 'MySQL Connection failed: ' . $ex->getMessage();  
		} 
		
	}