<?php 
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."api".DIRECTORY_SEPARATOR."authenticate.php");
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."config.php");
	$key=@$_POST["key"];
	if(checkIdentity($key)<1)
	{
		echo "Failure! You don't permssion to view shouts";
		
	} else
	{
		try
		{
			if(empty($_POST["viewRadius"]))
			{
				$viewRadius = 0;
				
			}
			else
			{
				$viewRadius = (int) $_POST["viewRadius"];
			}
			
			if(empty($_POST["latitude"]))
			{
				throw new InvalidArgumentException('The latitude given is invalid');
			}
			else
			{
				$latitude = (double) $_POST["latitude"];
			}
			
			if(empty($_POST["longitude"]))
			{
				throw new InvalidArgumentException('The longitude given is invalid');
			}
			else
			{
				$longitude = (double) $_POST["longitude"];
			}
			
			$viewRadius = $viewRadius * $viewRadius;
			$latitude = deg2rad($latitude);
			$longitude = deg2rad($longitude);
			$latSin = sin ($latitude);
			$latCos = cos ($latitude);

			$db = new PDO("mysql:dbname=$db_database;host=$db_server", $db_username, $db_password);
			
			
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$usersQuery = $db->prepare("SELECT id, name, username FROM $db_userTable");  
			$usersQuery->execute();
			$users = $usersQuery->fetchAll();
			$userArray = array();
			
			foreach($users as $user){
				//echo $user["id"];
				//echo "               <br />". $user["name"];			
				$userArray[$user["id"]] = $user["name"];
			}
			
			
			
			$selectQuery = $db->prepare("SELECT * FROM $db_postTable WHERE timestampdiff(SECOND,expirytime, now() ) < 0 
										ORDER BY `postTime` DESC");  
			$data = array(  'longitude' => $longitude, 'latSin' => $latSin, 'latCos' => $latCos, 'viewRadius' => $viewRadius );
//var_dump($data);			
			$selectQuery->execute($data); 
			
			$selectQuery->setFetchMode(PDO::FETCH_ASSOC);  
			//echo "hi";
			$shouts = array();
			//var_dump($users);
			while($row = $selectQuery->fetch()) {  
				$lat1 = $latitude;
				$lon1 = $longitude;
				$lat2 = $row["latitude"];
				$lon2 = $row["longitude"];
				
				$R = 6371000; // km
				$dLat = ($lat2-$lat1);
				$dLon = ($lon2-$lon1);

				$a = sin($dLat/2) * sin($dLat/2) +
						sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2); 
				$c = 2 * atan2(sqrt($a), sqrt(1-$a)); 
				$d = $R * $c;
				
				//echo $d . "<br /> <br />";
				
				if($viewRadius>$d &&  ($row['viewRadius']==0 || $row['viewRadius'] > $d)  )
				{
					$row["latitude"] = rad2deg ($lat2);
					$row["longitude"] = rad2deg ($row["longitude"]);
					$row["message"] = htmlspecialchars($row["message"]);
					
					$row["name"] = htmlspecialchars($userArray[$row["poster"]]);	

					array_push($shouts,$row);	
				}
			}  
			
			echo json_encode($shouts);
		}
		catch (InvalidArgumentException $ex)
		{
			echo 'Invalid Input value: ' . $ex->getMessage();  
		} 
		catch (PDOException $ex) 
		{
		  echo 'MySQL Connection failed: ' . $ex->getMessage();  
		}
	}
?>