<?php
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."api".DIRECTORY_SEPARATOR."authenticate.php");
	
	if(checkIdentity($key)<1)
	{
		echo "You don't permssion to create Users";
	}
	else
	{
		require(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."config.php");
		
		
		
		try {
			if(empty($_GET["name"])||!ctype_alnum($_GET["name"]))
			{
				throw new InvalidArgumentException('The Name given is invalid');
			}
			if(empty($_GET["username"])||!ctype_alnum($_GET["username"]))
			{
				throw new InvalidArgumentException('The Username is invalid');
			}
			if(empty($_GET["password"])||!ctype_alnum($_GET["password"]))
			{
				throw new InvalidArgumentException ('The password is invalid');
			}
			if(!filter_var($_GET["email"], FILTER_VALIDATE_EMAIL))
			{
				throw new InvalidArgumentException ('The email is invalid');
			}
			
			$name = $_GET["name"];
			$username = $_GET["username"];
			$password = $_GET["password"];
			$email = $_GET["email"];
			
			$username=strtolower($username);
			
			
			$db = new PDO("mysql:dbname=$db_database;host=$db_server", $db_username, $db_password);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$selectQuery = $db->prepare("SELECT `username`, `email` FROM $db_userTable WHERE username = :username OR email = :email");  
			$data = array( 'username' => $username,  'email' => $email ); 
			echo $selectQuery->execute($data); 
			
			$selectQuery->setFetchMode(PDO::FETCH_ASSOC);  
			
			while($row = $selectQuery->fetch()) {  
				if($row['username']==$username)
				{
					throw new InvalidArgumentException ('The Username Entered is already in use.');
				}
				if($row['email']==$email)
				{
					throw new InvalidArgumentException ('The email is already associated with another account.');
				}
				throw new InvalidArgumentException ('Something went wrong');
			}  
			
			$data = array( 'username' => $username, 'password' => $password, 'email' => $email, 'name' => $name ); 
			$insertQuery = $db->prepare("INSERT INTO $db_userTable ( username, password, email, name ) values ( :username, :password, :email, :name )");  
			if($insertQuery->execute($data) == 1)
			{
				echo "User " . $username . " added successfully.";
			}
			
		} catch (InvalidArgumentException $ex)
		{
			echo 'Invalid Input value: ' . $ex->getMessage();  
		} catch (PDOException $ex) 
		{
		  echo 'MySQL Connection failed: ' . $ex->getMessage();  
		}
		
		
	}
	

?>