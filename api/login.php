<?php
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."api".DIRECTORY_SEPARATOR."authenticate.php");
	$key=@$_POST["key"];
	if(checkIdentity($key)<1)
	{
		echo "You don't permssion to create Users";
		
	} else
	{
		try 
		{
			if(empty($_POST["username"])||!ctype_alnum($_POST["username"]))
			{
				throw new InvalidArgumentException('The username given is invalid');
			}
			if(empty($_POST["password"])||!ctype_alnum($_POST["password"]))
			{
				throw new InvalidArgumentException('The password is invalid');
			}
			
			$username = $_POST["username"];
			$password = $_POST["password"];
			
			$username=strtolower($username);
			
			require(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."config.php");
			
			$db = new PDO("mysql:dbname=$db_database;host=$db_server", $db_username, $db_password);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$selectQuery = $db->prepare("SELECT `username`, `password` FROM $db_userTable WHERE username = :username");  
			$data = array( 'username' => $username); 
			$selectQuery->execute($data); 
			
			$selectQuery->setFetchMode(PDO::FETCH_ASSOC);  
			
			$login = false;
			require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR."password.php");
			while($row = $selectQuery->fetch()) {  
				if($row['username']==$username)
				{
					
					if(password_verify($password,$row["password"]))
					{
						$login=true;
					}
				}
			}  
			
			if($login)
			{
				$authToken = uniqid($username, true);
				$updateQuery = $db->prepare("Update $db_userTable SET authToken = :authToken WHERE username = :username");  
				$data = array( 'authToken' => $authToken, 'username' => $username); 
				if($updateQuery -> execute($data) == 1)
				{
					echo "Success! ".$authToken;
				}
				else
				{
					throw new PDOException('Something went wrong. Auth Token couldn\'t be stored');
				}

			}
			else
			{
				echo "Failure! Username or Password is Incorrect";
			}
			
			
		} catch (InvalidArgumentException $ex)
		{
			echo 'Invalid Input value: ' . $ex->getMessage();  
		} catch (PDOException $ex) 
		{
		  echo 'MySQL Connection failed: ' . $ex->getMessage();  
		} 
	}