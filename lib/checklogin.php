<?php
	require_once(__DIR__.DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."config.php");
			
	function checklogin($authToken="")
	{
		global $db_server, $db_username, $db_password, $db_database, $db_userTable, $db_postTable;
		
		if(empty($authToken))
			if(isset($_COOKIE['megaAuthToken']))
			{
				$authToken=$_COOKIE['megaAuthToken'];
			}
			else
			{
				return false;
			}
		
		$db = new PDO("mysql:dbname=$db_database;host=$db_server", $db_username, $db_password);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$selectQuery = $db->prepare("SELECT `id`,`name`,`email`, `username` FROM $db_userTable WHERE authToken = :authToken");  
		$data = array( 'authToken' => $authToken); 
		$selectQuery->execute($data); 
		
		$selectQuery->setFetchMode(PDO::FETCH_ASSOC);  
		
		while($row = $selectQuery->fetch()) {  
				return $row;
		}  
		
		return false;
		
		
		
	}
?>