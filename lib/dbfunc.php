<?php
function getMySQLUsers()
{
	require(__DIR__."../config/database.php");
	
	try {
	  $db = new PDO("mysql:dbname=$db_userTable;host=$db_server", $db_username, $db_password);
	} catch (PDOException $ex) {
	  echo 'MySQL Connection failed: ' . $ex->getMessage();  
	}
	
	return $db;
}

function getMySQLPosts()
{
	require(__DIR__."../config/database.php");
	
	try {
	  $db = new PDO("mysql:dbname=$db_postTable;host=$db_server", $db_username, $db_password);
	} catch (PDOException $ex) {
	  echo 'MySQL Connection failed: ' . $ex->getMessage();  
	}
	
	return $db;
}
?>
